<?php  
session_start();

//Voy a configurar DATOS de mi web
$midirectorio='imagenes';

//Llamo al archivo de funciones
require('includes/funciones.php');

//Llamo a archivos importantes a incluir
require('includes/conexion.php');

require('includes/login_control.php');
//Recogemos el fichero que cargaremos en el contenedor
if(isset($_GET['pag'])){
	$pag=$_GET['pag'];
}else{
	$pag='inicio.php';
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Plantilla básica de Bootstrap</title>
 
    <?php //CSS de Bootstrap; ?>
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">

    <?php //CSS Propio; ?>
    <link href="css/propio.css" rel="stylesheet" media="screen">
  </head>
  <body>
    <section class="container">

    	<header><?php require('includes/encabezado.php'); ?></header>

      <nav><?php require('includes/menu.php'); ?></nav>
      <nav><?php require('includes/login.php'); ?></nav>

  		<section class="row">
  			<section>
  				<?php require('paginas/'.$pag); ?>
  			</section>
  		</section>

    	<footer><?php require('includes/pie.php'); ?></footer>

    </section>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
<?php  
// 5.- Desconectar
$conexion->close();
?>