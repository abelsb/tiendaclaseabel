-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-01-2018 a las 20:57:15
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `idProducto` int(11) NOT NULL,
  `nombreProducto` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `precioProducto` float NOT NULL,
  `cantidadProducto` int(11) NOT NULL,
  `imagenProducto` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `fechaProducto` datetime NOT NULL,
  `descripcionProducto` longtext COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idProducto`, `nombreProducto`, `precioProducto`, `cantidadProducto`, `imagenProducto`, `fechaProducto`, `descripcionProducto`) VALUES
(2, 'cacahuete de temporada', 4, 20, '', '2018-01-09 00:00:00', 'Descripcion de los cachuetes'),
(3, 'Pistacho fresco', 7, 30, '', '2018-01-09 00:00:00', 'Descripcion del producto de tipo pistacho'),
(4, 'Pepinillo de fuentes', 6, 12, '', '2018-01-09 00:00:00', 'Descripcion de este jugoso vinagrillo!'),
(5, 'Tarta de arandanos', 23, 1, '', '0000-00-00 00:00:00', ''),
(6, 'Pistaños agrios', 45, 8, '', '0000-00-00 00:00:00', ''),
(8, 'producto de muestra ', 23, 7, '', '0000-00-00 00:00:00', 'La cescripcion q euq sadasd '),
(9, 'prueba', 12, 23, '1516127256101.jpg', '0000-00-00 00:00:00', 'La descripcion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL,
  `nombreUsuario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `claveUsuario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `sesionUsuario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `correoUsuario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `activadoUsuario` tinyint(1) NOT NULL DEFAULT '0',
  `codigoCorreoUsuario` varchar(150) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsuario`, `nombreUsuario`, `claveUsuario`, `sesionUsuario`, `correoUsuario`, `activadoUsuario`, `codigoCorreoUsuario`) VALUES
(1, 'admin', '81dc9bdb52d04dc20036dbd8313ed055', '113d5a0e8a727472c0f4e34b4281a3cc', '', 1, ''),
(2, 'invitado', '81dc9bdb52d04dc20036dbd8313ed055', '8d3242535659920c6e35b0dfcaa62704', '', 1, ''),
(3, 'david', '81dc9bdb52d04dc20036dbd8313ed055', '', 'davidfraj@gmail.com', 0, '4e324be6d209596526c3bea242da5ff6'),
(4, 'david2', '81dc9bdb52d04dc20036dbd8313ed055', '', 'davidfraj2@gmail.com', 0, '6d33c4ddd8453f87af695eb6a6ecf5f8'),
(5, 'david3', '81dc9bdb52d04dc20036dbd8313ed055', '', 'davidfraj3@gmail.com', 1, ''),
(6, 'david4', '81dc9bdb52d04dc20036dbd8313ed055', '', 'davidfraj4@gmail.com', 1, '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idProducto`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `idProducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
