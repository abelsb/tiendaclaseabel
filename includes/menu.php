<?php  
//Me creo un array, con los archivos
if($_SESSION['conectado']){
	$archivos=['inicio.php','insertar.php','sugerencias.php'];
	$titulos=['Ir a Inicio','Insertar','Sugerencias'];
}else{
	$archivos=['inicio.php'];
	$titulos=['Ir a Inicio'];
	
}


//Y otro array con los titulos de los enlaces


?>


<nav class="navbar navbar-default" role="navigation">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse"
            data-target=".navbar-ex1-collapse">
      <span class="sr-only">Desplegar navegación</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
  </div>
 
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
		<?php  
		//Creamos un bucle para generar los enlaces de la web
		//Si el enlace, coincide con la pagina que se esta mostrando
		//Aplicamos la class active al li correspondiente
		for ($i=0; $i < count($archivos); $i++) { 
			if($archivos[$i]==$pag){
				$clase='active';
			}else{
				$clase='';
			}
			?>
			<li class="<?php echo $clase; ?>">
				<a href="index.php?pag=<?php echo $archivos[$i]; ?>">
					<?php echo $titulos[$i]; ?>
				</a>
			</li>
			<?php
		}
		?>
    </ul>
  </div>
</nav>
